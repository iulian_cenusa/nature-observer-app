# Nature Observer App

This project aims to create an web application in which the user can add and organize observations about nature, plants and animals.

It is inspired by [iNaturalist](https://www.inaturalist.org) which is a great social network of naturalists, citizen scientists and biologist built on the concept of mapping and sharing observations of biodiversity across the globe.

![Dashboard](./images/dashboard.png)

# Getting Started 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).\
For the backend the project use a server created with NodeJS and [Express](https://www.npmjs.com/package/express). The data is saved in JSON format in a file but can be changed to connect to a Database.

Also this project uses [React Icons](https://react-icons.github.io/react-icons/).

## Available Scripts

In the project directory, you can run:

## `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## `npm run server`

Runs the backend server on port `5000`. Use an API client to send requests on  [http://localhost:5000/observations](http://localhost:5000/observations) if you want to test it. \
On this server you can find an alternative upload form at  [http://localhost:5000](http://localhost:5000) because the one from frontend (React component) is not working properly.

![Alternatice upload form](./images/single_image_upload.png)

# Testing

For testing purposes you can use [Thunder client](https://www.thunderclient.io/) for VS code. 

![API Thunder Example](./images/API_thunder_client.png)