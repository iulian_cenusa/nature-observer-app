const express = require('express')
const app = express()
const fs = require('fs');
const { v4 } = require('uuid');
const cors = require('cors')
const multer = require('multer')
const path = require('path')

require('dotenv').config()

app.use(express.json())
app.use(cors({
    origin: "http://localhost:3000"
}))

app.use((error, req, res, next) => {
    console.log('This is the rejected field ->', error.field);
  });

// View Engine Setup
app.set("views",path.join(__dirname,"views"))
app.set("view engine","ejs")

// set storage engine 
const storage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null, './public/img/')
    },
    filename: (req,file,cb) => {
        cb(null,Date.now() + file.originalname)
    }

})

// variables
const PORT = process.env.BACKEND_PORT || 5000
global.observations = []
const upload = multer({
    storage: storage
})

// Upload test cleint 
app.get("/",function(req,res){
    console.log(req)
    res.render("upload_client");
})

// Get observations
app.get('/observations', (req,res) => {

    try {

        const data = fs.readFileSync('./observations.json', 'utf8');
        observations = JSON.parse(data);
    
    } catch (err) {
        console.error(`Error reading file from disk: ${err}`);
    }

    res.status(200).send(observations)
})

app.get('/observations/:id', (req,res) => {

    try {

        const data = fs.readFileSync('./observations.json', 'utf8');
        observations = JSON.parse(data);
    
    } catch (err) {
        console.error(`Error reading file from disk: ${err}`);
    }

    const observation = observations.find( obj => obj.id == req.params.id )
    if (!observation) res.status(404).send('Observation not found')
    else res.status(200).send(observation)
})

// Add observation
app.post('/observations', upload.single('nature-image') , (req,res) =>{

    const observation = {
        id: v4(),
        title: req.body.title,
        desc: req.body.desc,
        filename: req.file.filename
    }
    observations.push(observation)
    res.send(observation)

    fs.writeFile('./observations.json', JSON.stringify(observations, null, 4), (err) => {
        if (err) {
            console.log(`Error writing file: ${err}`);
        }
    });

})

// Delete observation
app.delete('/observations/:id', (req,res) => {

    try {

        const data = fs.readFileSync('./observations.json', 'utf8');
        observations = JSON.parse(data);
    
    } catch (err) {
        console.error(`Error reading file from disk: ${err}`);
    }

    const observation = observations.find( obj => obj.id == req.params.id )
    if (!observation) res.status(404).send('Observation not found')
    else {
        
        for( var i = 0; i < observations.length; i++){ 
    
            if (observations[i] == observation){
                observations.splice(i, 1)
            }
        
        }

        res.status(200).send(observation)
        fs.writeFile('./observations.json', JSON.stringify(observations, null, 4), (err) => {
            if (err) {
                console.log(`Error writing file: ${err}`);
            }
        });
    }
    
})

app.listen(PORT, () =>{ console.log(`\nListening on port ${PORT}`) })
