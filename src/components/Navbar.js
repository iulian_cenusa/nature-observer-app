
function Navbar({ onClick }) {

    return (
        <>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/">Profile</a></li>
                <li><a href="/">All observations</a></li>
            </ul>
            <button className='btn' onClick={onClick} >Add Observation</button>
        </>
    )
}

export default Navbar
