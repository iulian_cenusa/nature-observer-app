// Add Observation component

import { useState } from 'react'

function AddObservation({ onAdd }) {

    const [ title , setTitle ] = useState('')
    const [ desc , setDesc ] = useState('')
    const [file,setFile ] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()

        if (!title) {
            alert('Please add title!')
            return
        }

        onAdd({title,desc,file})

        setTitle('')
        setDesc('')
        setFile('')

    }

    return (
        <form className='grid-item' onSubmit={onSubmit} encType="multipart/form-data">
            <div className='form-control'>
                <label>Add new observation</label>
                <input type='text' placeholder='Name of animal/insect/plant (required)' value={title} onChange={(e) => setTitle(e.target.value)} name="title" />
                <input type='text' placeholder='Description (obtional)' value={desc} onChange={(e) => setDesc(e.target.value)} name="desc" />
                <input type='file' name="nature-image" onChange={(e) => {
                    const file = e.target.files[0];
                    setFile(file)
                }}/>
                <input type='submit' value='Save' />

            </div>
            
        </form>
    )
}

export default AddObservation
