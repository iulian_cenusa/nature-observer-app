// Observation component

import { FaTimes } from 'react-icons/fa'

function Observation({observation , onDelete}) {

    return (
        <div className='grid-item'>
            <img src={`/img/${observation.filename}`} alt={'imagine'} height={'200'} width={'250'}/>
            <h2 className='title'> {observation.title}</h2>
            {observation.desc && <p> <b>Description:</b> {observation.desc}</p>}
            <FaTimes styles={'background-color:red'} onClick = {() => onDelete(observation.id)}/> 
        </div>
    )
}

export default Observation
