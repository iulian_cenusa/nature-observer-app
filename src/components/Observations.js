import Observation from './Observation'

function Observations({ observations , onDelete} ) {
    return (
        <>
          {observations.map((ob) => (
              <Observation key={ob.id} observation ={ob} onDelete={onDelete} />
          ) )}  
        </>
    )
}

export default Observations
