import Observations from './components/Observations'
import Navbar from './components/Navbar'
import AddObservation from './components/AddObservation'

import {useState, useEffect} from 'react'
const axios = require('axios').default;

function App() {

  const [obs,setObs] = useState([])   
  const [showAdd , setShowAdd] = useState(false)      

  useEffect(

    () =>{

      const getObservations = async () =>{
        const obsFronServer = await fetchObservations()
        setObs(obsFronServer)
      }

      getObservations()

    }, []
  )

    // fetch observations
    const fetchObservations = async () => {
      const res = await fetch('http://localhost:5000/observations')
      const data = await res.json()
  
      return data
    }

    // detele task

const deleteObservation = async (id) => {

  await fetch(`http://localhost:5000/observations/${id}` , {method: 'DELETE'})

  setObs( obs.filter( (ob) => ob.id !== id ) )
} 

// Add observation
const addObservation = async (ob) => {

  console.log(ob)

  axios.post('http://localhost:5000/observations', {
    title: ob.title,
    desc: ob.desc,
    file: ob.file
  })
  .then((response) => {
    console.log(response);
  })
  .catch((error) => {
    console.log(error);
  });

  /*const res = await fetch('http://localhost:5000/observations',
  {
    method: 'POST',
    headers: {'Content-type': 'multipart/form-data'},
    body: ob
  })

  const data = await res.json()

  console.log(data)*/

  setObs([...obs,ob])
}

  return (
    <>
    <div className='navbar'>
      <Navbar onClick={ ()=>setShowAdd(!showAdd)} />
    </div>
      <div className='grid-container'>
        { obs.length > 0 ? <Observations observations={obs} onDelete={deleteObservation} /> : '' }
        
        { showAdd && <AddObservation onAdd={addObservation} showAdd={showAdd}/>}

      </div>
    </>
  );
}

export default App;
